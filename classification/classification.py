import os
import numpy as np

from model.activity import Activity
from utils.file_utils import lines_in_file, appendable_file, get_data_matrix

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC, LinearSVC

from sklearn.externals import joblib

# Define the number of features in a data vector.
FEATURE_COLUMNS = 7

# Define the data directories.
FEATURES_DIR = "data/features"
SOURCES_DIR = "data/source"

# Define the target values for each activity.
activityID = {
    "Walking": 10,
    "Jogging": 20,
    "Cycling": 30,
    "Writing": 40,
    "Typing": 50,
    "Sitting": 60,
    "Standing": 70,
    "On Phone (sit)": 80,
    "On Phone (stand)": 90
}

activityName = {
    10: "Walking",
    20: "Jogging",
    30: "Cycling",
    40: "Writing",
    50: "Typing",
    60: "Sitting",
    70: "Standing",
    80: "On Phone (sit)",
    90: "On Phone (stand)"
}


def get_activity_id(activity_name):
    return activityID[activity_name]


def get_activity_name(activity_id):
    return activityName[activity_id]


# Define the bin size for raw data vectors.
BIN_SIZE = 10000


def main():
    # Mimic new training data. This will print accuracies for the classifiers given the new data.
    on_new_training_data(data_type="activities", print_results=True)


# Returns the model used to classify, and the classification result.
def classify_feature(data, model=None):
    if model is None:
        model = joblib.load("../model/classifier.txt")

    return [model, model.predict(X=data)]


def classify_raw(raw_data):
    line_count = len(raw_data.splitlines())

    # Create a matrix to hold the trial data.
    trial_matrix = np.ndarray((line_count, 4))

    # Read the trial data into the matrix.
    line_index = 0
    for line in raw_data.splitlines():
        data = np.array(line.split(","))

        # Check if we've reached the end of the file.
        if len(data) == 0 or len(data) == 1:
            break

        trial_matrix[line_index] = data
        line_index += 1

    # Check if the data just read was valid.
    if line_index == 0:
        print("Invalid file.")
        return None

    # Divide the trial data into smaller samples of the activity.
    # Keep track of how much data has been assigned.
    data_covered = 0
    end_time = int(trial_matrix[0][0])

    model = None
    features = []
    time_intervals = []

    # Loop until all activity data is grouped by time period.
    while data_covered < line_count:
        start_time = end_time
        end_time = start_time + 5000

        # Work out the number of data points in the time period.
        data_count = 0
        for data in enumerate(trial_matrix, data_covered):
            if data[1][0] < end_time:
                data_count += 1
            else:
                break

        # Create a matrix to hold the sample of data.
        section_matrix = np.ndarray((data_count, 4))
        section_start = trial_matrix[data_covered][0]
        section_index = 0

        # Add data from the time period interval to the sample.
        for data in enumerate(trial_matrix, data_covered):
            if data[1][0] < end_time:
                section_matrix[section_index] = data[1]
                section_index += 1
                data_covered += 1
            else:
                break

        # Get features for the sample.
        features.append(extract_features(section_matrix))

        # Keep track of the start and end time for this feature.
        time_intervals.append([section_start, section_matrix[section_index - 1][0]])

    # Classify the features.
    activities = []
    for feature, time_interval in zip(features, time_intervals):
        [model, activity] = classify_feature([feature], model)

        # Create activity objects.
        name = get_activity_name(activity[0])
        start_time = time_interval[0]
        end_time = time_interval[1]
        activities.append(Activity(name, start_time, end_time, 0))

    return activities


# Call when new labelled data is available.
def on_new_training_data(data_type="activities", print_results=False):
    # Extract features from the new data.
    get_features(data_type)

    # Find the best classifier for this new data.
    classifier = get_best_classifier(data_type=data_type, print_results=print_results)
    print("Best classifier is {}".format(classifier.__class__.__name__))

    # Save the classifier model to a file so that we can load it when needed.
    joblib.dump(classifier, "../model/classifier.txt")


# Extracts features from the raw data stored in data/source/'data_type'.
def get_features(data_type):
    print("Starting feature extraction.")

    # Create a directory to store feature data.
    features_directory = "{}/{}".format(FEATURES_DIR, data_type)
    if not os.path.exists(features_directory):
        os.makedirs(features_directory)

    # Create files that features can be written to.
    train_file = appendable_file("{}/{}".format(features_directory, "train.csv"))
    test_file = appendable_file("{}/{}".format(features_directory, "test.csv"))

    # 80% of features are for training, 20% for testing.
    feature_index = 0

    # Define which columns we want from the raw data.
    if data_type == "activities":
        column_indexes = [0, 1, 2, 3]
        user_dir_flag = "user_"
        delimiter = ","
    else:
        column_indexes = [0, 3, 4, 5]
        user_dir_flag = "U"
        delimiter = None

    # Iterate through the dataset's users.
    base_dir_name = "{}/{}".format(SOURCES_DIR, data_type)
    for user_dir_name in os.listdir(base_dir_name):

        # Find the user directories.
        if user_dir_name.startswith(user_dir_flag):

            # Iterate through the user's activities.
            for activity_name in os.listdir("{}/{}".format(
                    base_dir_name, user_dir_name)):

                # Iterate through the activity trials.
                for trial_filename in os.listdir("{}/{}/{}".format(
                        base_dir_name, user_dir_name, activity_name)):

                    # Open the trial file.
                    trial_file = open("{}/{}/{}/{}".format(
                        base_dir_name, user_dir_name, activity_name, trial_filename))

                    # Get the number of accelerometer readings in the trial.
                    line_count = lines_in_file(trial_file)

                    # Create a matrix to hold the trial data.
                    trial_matrix = np.ndarray((line_count, 4))

                    # Read the trial data into the matrix.
                    line_index = 0
                    for line in trial_file:
                        data = np.array(line.split(delimiter))

                        # Check if we've reached the end of the file.
                        if len(data) == 0 or len(data) == 1:
                            break

                        trial_matrix[line_index] = data[column_indexes]
                        line_index += 1

                    # Close the file connection.
                    trial_file.close()

                    # Check if the data just read was valid.
                    if line_index == 0:
                        print("Invalid file: {}".format(trial_filename))
                        continue

                    # Divide the trial data into smaller samples of the activity.
                    # Keep track of how much data has been assigned.
                    data_covered = 0
                    end_time = int(trial_matrix[0][0])

                    # Loop until all activity data is grouped by time period.
                    while data_covered < line_count:
                        start_time = end_time
                        end_time = start_time + BIN_SIZE

                        # Work out the number of data points in the time period.
                        data_count = 0
                        for data in enumerate(trial_matrix, data_covered):
                            if data[1][0] < end_time:
                                data_count += 1
                            else:
                                break

                        # Create a matrix to hold the sample of data.
                        section_matrix = np.ndarray((data_count, 4))
                        section_index = 0

                        # Add data from the time period interval to the sample.
                        for data in enumerate(trial_matrix, data_covered):
                            if data[1][0] < end_time:
                                section_matrix[section_index] = data[1]
                                section_index += 1
                                data_covered += 1
                            else:
                                break

                        # Get features for the sample.
                        features = extract_features(section_matrix)

                        # Save the feature vector to a file, with the appropriate target value.
                        feature = "{},{},{},{},{},{},{}\n".format(
                            features[0], features[1], features[2], features[3], features[4], features[5],
                            get_activity_id(activity_name) if data_type == "activities" else activity_name)

                        # Write 20% of the features to a testing file, 80% to training.
                        if feature_index % 5 == 0:
                            test_file.write(feature)
                        else:
                            train_file.write(feature)
                        feature_index += 1
    # Close the files.
    train_file.close()
    test_file.close()


def extract_features(data_sample):
    # Compute the mean of the sample.
    means = np.mean(data_sample[:, range(1, 4)], axis=0)

    # Compute the standard deviation of the sample.
    stds = np.std(data_sample[:, range(1, 4)], axis=0)

    # Return the feature array.
    return np.append(means, stds)


# Returns all classifiers that we will trial.
def get_classifiers():
    return [
        QuadraticDiscriminantAnalysis(),
        DecisionTreeClassifier(),
        RandomForestClassifier(),
        KNeighborsClassifier(),
        AdaBoostClassifier(),
        MLPClassifier(hidden_layer_sizes=(150, 150, 100)),
        GaussianNB(),
        LinearSVC(),
        SVC(),
    ]


# Returns the best performing classifier for the current training and testing data.
def get_best_classifier(data_type="activities", print_results=False):
    print("Finding best classifier.")
    classifiers = get_classifiers()

    # Load the training and testing data.
    train_data = get_data_matrix("{}/{}/train.csv".format(FEATURES_DIR, data_type), FEATURE_COLUMNS)
    test_data = get_data_matrix("{}/{}/test.csv".format(FEATURES_DIR, data_type), FEATURE_COLUMNS)
    test_data_count = len(test_data)

    # Train the networks.
    for classifier in classifiers:
        print("Training {}".format(classifier.__class__.__name__))
        classifier.fit(train_data[:, range(FEATURE_COLUMNS - 1)], train_data[:, FEATURE_COLUMNS - 1])

    # Measure their accuracies using the test data.
    classifier_accuracies = list()
    for classifier in classifiers:
        # Get the classifier to predict results.
        results = classifier.predict(X=test_data[:, range(FEATURE_COLUMNS - 1)])

        # Compare the prediction to the target.
        no_correct = 0
        for index in range(test_data_count):
            if test_data[index, FEATURE_COLUMNS - 1] == results[index]:
                no_correct += 1

        # Calculate a percentage accuracy.
        accuracy = (no_correct / float(test_data_count)) * 100
        classifier_accuracies.append(accuracy)

        # Print the results.
        if print_results:
            print("{}% {} accuracy.".format(accuracy, classifier.__class__.__name__))

    # Return the classifier with the highest accuracy.
    return classifiers[classifier_accuracies.index(max(classifier_accuracies))]


# Main function to invoke the program.
if __name__ == "__main__":
    main()
