import numpy as np


# Clears any existing file and returns a new, appendable file.
def appendable_file(filename):
    open(filename, 'w+').close()
    return open(filename, 'a')


# Returns the number of lines in a file.
def lines_in_file(file):
    line_count = 0
    for _ in file:
        line_count += 1
    file.seek(0)
    return line_count


# Returns the data held in a file as a numpy multidimensional array.
def get_data_matrix(filename, columns):
    # Open the file.
    file = open(filename)

    # Work out the number of feature vectors.
    data_count = lines_in_file(file)

    # Preallocate the matrix. We know there are 6 features and a target value.
    data_matrix = np.ndarray((data_count, columns))

    # Load the data from the file to the matrix.
    line_index = 0
    for line in file:
        data_matrix[line_index] = line.split(',')
        line_index += 1

    # Close the connection to the file.
    file.close()

    # Return the data.
    return data_matrix
