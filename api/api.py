from classification.classification import classify_raw
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route("/classify", methods=["POST"])
def classify_new_data():
    print("reached endpoint. Method = {}. Data = {}".format(request.method, request.form))

    if "acceleration" in request.form:

        # Get the acceleration data.
        data = request.form["acceleration"]

        # Classify activities in the data.
        activities = classify_raw(data)

        # Return the activities in json.
        activity_list = [activity.serialize() for activity in activities]
        return jsonify({"activities": activity_list})
    else:
        return "error"


if __name__ == "__main__":
    app.run(debug=True, use_debugger=False, use_reloader=False)
