class Activity:

    name = None
    start_time = None
    end_time = None
    confidence = None

    def __init__(self, name, start_time, end_time, confidence):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.confidence = confidence

    def serialize(self):
        return {
            "name": self.name,
            "start_time": self.start_time,
            "end_time": self.end_time,
            "confidence": self.confidence
        }
